## Build

Install dependencies

`npm install`

Build

`npm run build`

## Run

`node build/app.js -l "1,2,5,12,1,3,6,12,53,2,7,8,22,34,1,8,3,6" -h 12`

## Test

Run all tests.

`npm run test`
