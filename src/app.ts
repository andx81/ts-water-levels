import waterLevels from "./algorithm"

const argv = require("minimist")(process.argv.slice(2))

const landscape: number[] = argv.l ? argv.l.split(",").map((v: String) => parseInt(v.trim(), 10)) : []
const numHours = argv.h ? parseInt(argv.h) : 0

if (!landscape || !numHours || landscape.find(isNaN) !== undefined) {
  console.warn("Error: wrong arguments!")

  console.log('\nUsage:\n\nnode app.js -l "1, 2, 3, 4, 5" -h 2 [-p 0.000001]')

  process.exit()
}
const precision = argv.p && parseFloat(argv.p)

const segments = waterLevels(landscape, numHours, precision || 0.00001)

segments.forEach((v, i) => {
  console.log(
    "Segment #" +
      (i + 1) +
      ";\theight: " +
      v.height +
      ";\twater level: " +
      v.water +
      "; total level: " +
      (v.height + v.water)
  )
})
