export type Group = {
  items: number[]
  level: number
  water: number
}

export type Segment = {
  height: number
  water: number
}

const minLevelDiff = (groups: Group[], i: number): number => {
  if (i > 0 && i < groups.length - 1) {
    return Math.min(groups[i - 1].level - groups[i].level, groups[i + 1].level - groups[i].level)
  } else if (i > 0) {
    return groups[i - 1].level - groups[i].level
  } else if (i < groups.length - 1) {
    return groups[i + 1].level - groups[i].level
  }
  return 0
}

const fillWithWater = (groups: Group[], i: number, level: number, precision: number): boolean => {
  groups[i].level += level
  groups[i].water -= level * groups[i].items.length

  var moveLeft = false
  // If the level of the neighboring group is equal to the level of the current group, then merge these groups
  if (i < groups.length - 1 && Math.abs(groups[i].level - groups[i + 1].level) < precision) {
    const group = groups.splice(i + 1, 1)
    groups[i].items = groups[i].items.concat(group[0].items)
    groups[i].water += group[0].water
  }
  if (i > 0 && Math.abs(groups[i].level - groups[i - 1].level) < precision) {
    const group = groups.splice(i - 1, 1)
    groups[i - 1].items = group[0].items.concat(groups[i - 1].items)
    groups[i - 1].water += group[0].water
    moveLeft = true
  }

  return moveLeft
}

const drainWater = (groups: Group[], i: number): void => {
  if (
    i > 0 &&
    groups[i - 1].level < groups[i].level &&
    i < groups.length - 1 &&
    groups[i + 1].level < groups[i].level
  ) {
    const flow = Math.min(
      groups[i].water / 2,
      (groups[i].level - groups[i + 1].level) * groups[i + 1].items.length,
      (groups[i].level - groups[i - 1].level) * groups[i - 1].items.length
    )
    groups[i - 1].water += flow
    groups[i].water -= 2 * flow
    groups[i + 1].water += flow
  } else if (i > 0 && groups[i - 1].level < groups[i].level) {
    const flow = Math.min(groups[i].water, (groups[i].level - groups[i - 1].level) * groups[i - 1].items.length)
    groups[i].water -= flow
    groups[i - 1].water += flow
  } else if (i < groups.length - 1 && groups[i + 1].level < groups[i].level) {
    const flow = Math.min(groups[i].water, (groups[i].level - groups[i + 1].level) * groups[i + 1].items.length)
    groups[i].water -= flow
    groups[i + 1].water += flow
  }
}

function waterLevels(landscape: number[], numHours: number, precision: number = 0.0001): Segment[] {
  // Initial initialization
  // Group all segments by height, and distribute all water into these groups.
  var groups: Group[] = []
  for (let i = 0; i < landscape.length; i++) {
    if (groups.length && landscape[i] == groups[groups.length - 1].level) {
      groups[groups.length - 1].items.push(i)
      groups[groups.length - 1].water += numHours
    } else {
      groups.push({ items: [i], level: landscape[i], water: numHours })
    }
  }

  // Main algorithm loop
  while (true) {
    // Find the group with the minimum level (containing unallocated water)
    var hasWater = false
    for (let i = 0; i < groups.length; i++) {
      if (groups[i].water > precision) {
        const level = minLevelDiff(groups, i)
        if (level >= 0) {
          const ownAvailableWaterLevel = groups[i].water / groups[i].items.length
          // Fill the groups with water to the needed level or while we have water to fill.
          // If the level is 0, it means that the water has filled the entire landscape,
          // in this case we evenly fill it with the rest of the water
          const fillLevel = level ? Math.min(ownAvailableWaterLevel, level) : ownAvailableWaterLevel
          const moveLeft = fillWithWater(groups, i, fillLevel, precision)

          if (moveLeft) i--

          if (groups[i].water > precision) {
            hasWater = true
          }
        } else if (level < 0) {
          // Evenly drain the water on the adjacent lower levels
          drainWater(groups, i)
          hasWater = true
        }
      }
    }

    // if we have no water left, then we finish
    if (!hasWater) {
      break
    }
  }

  // Prepare result
  const toSegnet =
    (g: Group) =>
    (i: number): Segment => ({ height: landscape[i], water: g.level - landscape[i] })

  return groups.reduce((a: any[], g: Group) => [...a, ...g.items.map(toSegnet(g))], [])
}

export default waterLevels
