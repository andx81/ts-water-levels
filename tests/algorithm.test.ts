import { expect } from "chai"
import waterLevels from "../src/algorithm"

const precision = 0.00001

describe("Water level algorithm test", function () {
  const cases = [
    {
      name: "predefined test1",
      l: [1, 2, 3],
      h: 1,
      expect: [
        { height: 1, water: 2 },
        { height: 2, water: 1 },
        { height: 3, water: 0 },
      ],
    },
    {
      name: "two towers",
      l: [1, 10, 1, 1, 10, 1],
      h: 6,
      expect: [
        { height: 1, water: 9 },
        { height: 10, water: 0 },
        { height: 1, water: 9 },
        { height: 1, water: 9 },
        { height: 10, water: 0 },
        { height: 1, water: 9 },
      ],
    },
    {
      name: "predefined test3",
      l: [0, 3, 2, 1, 2, 3, 0],
      h: 1,
      expect: [
        { height: 0, water: 1.5 },
        { height: 3, water: 0 },
        { height: 2, water: 1 },
        { height: 1, water: 2 },
        { height: 2, water: 1 },
        { height: 3, water: 0 },
        { height: 0, water: 1.5 },
      ],
    },
    {
      name: "predefined test4",
      l: [1, 3, 2],
      h: 1,
      expect: [
        { height: 1, water: 2 },
        { height: 3, water: 0 },
        { height: 2, water: 1 },
      ],
    },
    {
      name: "should flow correctly1",
      l: [10, 100, 100, 10, 10],
      h: 20,
      expect: [
        { height: 10, water: 40 },
        { height: 100, water: 0 },
        { height: 100, water: 0 },
        { height: 10, water: 30 },
        { height: 10, water: 30 },
      ],
    },

    {
      name: "should flow correctly2",
      l: [0, 0, 50, 50, 100, 50, 0],
      h: 20,
      expect: [
        { height: 0, water: 45 },
        { height: 0, water: 45 },
        { height: 50, water: 0 },
        { height: 50, water: 0 },
        { height: 100, water: 0 },
        { height: 50, water: 0 },
        { height: 0, water: 50 },
      ],
    },
    {
      name: "city under water",
      l: [0, 50, 100, 50],
      h: 60,
      expect: [
        { height: 0, water: 110 },
        { height: 50, water: 60 },
        { height: 100, water: 10 },
        { height: 50, water: 60 },
      ],
    },
    {
      name: "downcountry under water",
      l: [1, 1, 1, 1],
      h: 10,
      expect: [
        { height: 1, water: 10 },
        { height: 1, water: 10 },
        { height: 1, water: 10 },
        { height: 1, water: 10 },
      ],
    },
    {
      name: "platform with ladder",
      l: [0, 1, 2, 3, 4, 5, 0],
      h: 2,
      expect: [
        { height: 0, water: 4.2 },
        { height: 1, water: 3.2 },
        { height: 2, water: 2.2 },
        { height: 3, water: 1.2000000000000002 },
        { height: 4, water: 0.20000000000000018 },
        { height: 5, water: 0 },
        { height: 0, water: 3 },
      ],
    },
  ]

  cases.forEach((t) => {
    it(t.name, function () {
      expect(waterLevels(t.l, t.h, precision)).to.eql(t.expect)
    })
  })

  it("random water level test", function () {
    for (let i = 0; i < 100; i++) {
      const landscape = genRandomLandscape(1, 100, 30)
      const numHours = Math.floor(Math.random() * 30)

      const result = waterLevels(landscape, numHours, precision)

      const totalWater = result.reduce((a, v) => a + v.water, 0)
      expect(Math.abs(totalWater - numHours * landscape.length) < precision).true
    }
  })

  it("big landscape test", function () {
    const landscape = genRandomLandscape(5000, 20000, 500)
    const numHours = Math.floor(Math.random() * 200)

    const result = waterLevels(landscape, numHours, precision)
    const totalWater = result.reduce((a, v) => a + v.water, 0)
    expect(Math.abs(totalWater - numHours * landscape.length) < precision).true
  })
})

function genRandomLandscape(minLen: number, maxLen: number, maxHeigh: number) {
  maxLen = minLen > maxLen ? minLen : maxLen
  const len = minLen + Math.floor(Math.random() * (maxLen - minLen))
  var landscape = []
  for (let i = 0; i < len; i++) {
    landscape.push(Math.floor(Math.random() * maxHeigh))
  }
  return landscape
}
